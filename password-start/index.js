// Copyright Istio Authors
//
//   Licensed under the Apache License, Version 2.0 (the 'License');
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an 'AS IS' BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

var http = require('http')
var axios = require('axios')
var dispatcher = require('httpdispatcher')
var fs = require('fs')

var port = parseInt(process.argv[2])

var healthy = true

var servicesDomain = (process.env.SERVICES_DOMAIN) ? `.${process.env.SERVICES_DOMAIN}` : ''


var checkServices = [{
    id: 'history',
    host: (process.env.HISTORY_HOSTNAME) ? process.env.HISTORY_HOSTNAME : 'history'
  },
  {
    id: 'list',
    host: (process.env.LIST_HOSTNAME) ? process.env.LIST_HOSTNAME : 'list'
  },
  {
    id: 'score',
    host: (process.env.RATING_HOSTNAME) ? process.env.RATING_HOSTNAME : 'rating'
  }
]

const indexFile = fs.readFileSync('index.html', 'utf8')


dispatcher.onGet('/', function (req, res) {

  res.writeHead(200, {
    'Content-type': 'text/html'
  })
  return res.end(indexFile)

})

dispatcher.onPost('/', async function (req, res) {
  try {
    request = JSON.parse(req.body)
  } catch (error) {
    res.writeHead(400, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      error: 'please provide valid request JSON'
    }))
    return
  }
  var response = {}

  await Promise.all(checkServices.map(async service => {
    var serviceResponse = await axios.post(`http://${service.host}${servicesDomain}:9080/password-check`, request)
    response[service.id] = serviceResponse.data.result
  }))
  res.writeHead(200, {
    'Content-type': 'application/json'
  })
  res.end(JSON.stringify(response))

})


dispatcher.onGet('/health', function (req, res) {
  if (healthy) {
    res.writeHead(200, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      status: 'Check is healthy'
    }))
  } else {
    res.writeHead(500, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      status: 'Check is not healthy'
    }))
  }
})


function handleRequest(request, response) {
  try {
    console.log(request.method + ' ' + request.url)
    dispatcher.dispatch(request, response)
  } catch (err) {
    console.log(err)
  }
}

var server = http.createServer(handleRequest)

server.listen(port, function () {
  console.log('Server listening on: http://0.0.0.0:%s', port)
})