cd password-history
docker build -t rahxam/history-v1:latest .
docker push rahxam/history-v1:latest
cd ../password-list
docker build -t rahxam/list-v1:latest .
docker push rahxam/list-v1:latest
cd ../password-rating
docker build -t rahxam/rating-v1:latest .
docker push rahxam/rating-v1:latest
cd ../password-start
docker build -t rahxam/start-v1:latest .
docker push rahxam/start-v1:latest