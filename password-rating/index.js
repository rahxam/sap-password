// Copyright Istio Authors
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

var http = require('http')
var dispatcher = require('httpdispatcher')

var port = parseInt(process.argv[2])

var healthy = true

var scores = []

var lengthScore = function (password) {
  return password.length * 4
}
scores.push(lengthScore)

var uppercaseScore = function (password) {
  const re = /[^A-Z]/g
  return (password.length - password.replace(re, '').length) * 2
}
scores.push(uppercaseScore)

var lowercaseScore = function (password) {
  const re = /[^a-z]/g
  return (password.length - password.replace(re, '').length) * 2
}
scores.push(lowercaseScore)

var numberScore = function (password) {
  const re = /[^\d]/g
  return password.replace(re, '').length * 4
}
scores.push(numberScore)

var specialScore = function (password) {
  const re = /[\dA-Za-z]/g
  return password.replace(re, '').length * 6
}
scores.push(specialScore)

var middleScore = function (password) {
  const re = /[A-Za-z]/g
  return password.substring(1, password.length - 1).replace(re, '').length * 2
}
scores.push(middleScore)

var lettersOnlyScore = function (password) {
  const re = /[A-Za-z]/g
  return password.replace(re, '').length === 0 ? password.length * -1 : 0
}
scores.push(lettersOnlyScore)

var numbersOnlyScore = function (password) {
  const re = /[\d]/g
  return password.replace(re, '').length === 0 ? password.length * -1 : 0
}
scores.push(numbersOnlyScore)

var repeatingScore = function (password) {
  const re = /(.)\1{1,}/g
  return (password.length - password.replace(re, '').length) * -1
}
scores.push(repeatingScore)

var consecutiveScore = function (password) {
  const re = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz)/ig
  return (password.length - password.replace(re, '').length) * -1
}
scores.push(consecutiveScore)


function calculateScore(password) {
  return scores.map(score => score(password)).reduce((a, b) => a + b)
}

dispatcher.onPost('/password-check', function (req, res) {
  try {
    request = JSON.parse(req.body)
  } catch (error) {
    res.writeHead(400, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      error: 'please provide valid request JSON'
    }))
    return
  }
  res.writeHead(200, {
    'Content-type': 'application/json'
  })
  res.end(JSON.stringify({
    result: calculateScore(request.password)
  }))
  return
})


dispatcher.onGet('/health', function (req, res) {
  if (healthy) {
    res.writeHead(200, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      status: 'Check is healthy'
    }))
  } else {
    res.writeHead(500, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      status: 'Check is not healthy'
    }))
  }
})


function handleRequest(request, response) {
  try {
    console.log(request.method + ' ' + request.url)
    dispatcher.dispatch(request, response)
  } catch (err) {
    console.log(err)
  }
}

var server = http.createServer(handleRequest)

server.listen(port, function () {
  console.log('Server listening on: http://0.0.0.0:%s', port)
})