// Copyright Istio Authors
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

var http = require('http')
var axios = require('axios')
var dispatcher = require('httpdispatcher')

var port = parseInt(process.argv[2])

var healthy = true


var passwordList = null

axios.get('https://pwlist.cfapps.eu10.hana.ondemand.com/passwords.txt')
  .then(function (response) {
    passwordList = response.data.split('\n')
  })
  .catch(function (error) {
    // handle error
    throw error
  });

dispatcher.onPost('/password-check', function (req, res) {
  var request
  try {
    request = JSON.parse(req.body)
  } catch (error) {
    res.writeHead(400, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      error: 'please provide valid request JSON'
    }))
    return
  }
  res.writeHead(200, {
    'Content-type': 'application/json'
  })
  res.end(JSON.stringify({
    result: passwordList.indexOf(request.password) > -1
  }))
  return
})


dispatcher.onGet('/health', function (req, res) {
  if (healthy) {
    res.writeHead(200, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      status: 'Check is healthy'
    }))
  } else {
    res.writeHead(500, {
      'Content-type': 'application/json'
    })
    res.end(JSON.stringify({
      status: 'Check is not healthy'
    }))
  }
})


function handleRequest(request, response) {
  try {
    console.log(request.method + ' ' + request.url)
    dispatcher.dispatch(request, response)
  } catch (err) {
    console.log(err)
  }
}

var server = http.createServer(handleRequest)

server.listen(port, function () {
  console.log('Server listening on: http://0.0.0.0:%s', port)
})